jQuery(document).ready(function ($) {
	var $ = jQuery.noConflict();

	/* ==========================================================
	   TABS FUNCTIONALITY 
	========================================================== */
    // vertical slider
	$('.vertical_slider').slick({
	  infinite: true,
	  slidesToShow: 6,
	  slidesToScroll: 1,
	  vertical: true,
	});
		
	$('.toggle-block').click(function(event){
        event.preventDefault();
        $('.navigation').toggle();
        $('#header').toggleClass("active");
		$('#header').removeClass("respons");
    });
     //$("nav ul li > ul").parent().prepend('<span></span>');
	 //$("nav ul li span").click(function() {
	  //$(this).parent().find('ul').slideToggle();
	  //$(this).parent().find('ul ul').hide();
	 //});
//	 $(".navigation .readmore-ul").click(function() {
//		$(this).parents(".navigation").addClass("act"); //добавляем класс текущей (нажатой)
//	 })
//		$(".navigation .close-ul").click(function() {
//		$(this).parents(".navigation").removeClass("act"); //добавляем класс текущей (нажатой)
//	 })

$('.navigation span').click(function() {
   $(this).siblings(".navigation ul").slideToggle();
   $(this).parent().toggleClass('active');
});


	function checkslider(){
	    if($(window).width() <= 767 && !($('.latest-news').hasClass('slick-initialized'))){ 
	    	$('.latest-news').slick({
	    		dots: true
	    	});
	    }else{
	    	if(($('.latest-news').hasClass('slick-initialized'))){
	    		$('.latest-news').slick('unslick');
	    	}
	    }
	}
	checkslider();
	$(window).resize(checkslider);
	
			$("#section4 #overseas").hover(function(){
				$("#section4").find('.center-block').removeClass("active");	
			});

			$("#section4 #china").hover(function(){
				$("#section4").find('.center-block').addClass("active");	
			});


			$(".chat-img a>img").on('click', function(e){
				e.preventDefault();
				if ( !$('.chat-img .pop_up').hasClass('open') ){
					$('.chat-img .pop_up').slideDown(300).addClass('open');
				} else {
					$('.chat-img .pop_up').slideUp(300).removeClass('open');
				}
			})

	//slider
	 $(".bg_slide_part").slick({
		infinite: true,
		speed: 1000,
		fade: true,
		autoplay: true,
		autoplaySpeed: 500,
		dots:true,
  responsive: [
    {
      breakpoint: 767,
      settings: {
        dots: true
      }
    }
  ]
	});
    $(".brand_slide").slick({
		infinite: true,
		speed: 1000,
		autoplay: false,
		autoplaySpeed: 500,
		
		  responsive: [
		    {
		      breakpoint: 767,
		      settings: {
		        dots: true
		      }
		    }
		  ]
	});
});