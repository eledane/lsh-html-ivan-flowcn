//Mobile
$(document).ready(function() {
  if(document.documentElement.clientWidth < 767){


jQuery(document).ready(function($) {
	var $bar,
	  tick,
	  isPause,
	  percentTime,
	  slideNumber = 0;
	
	//Banner slider
	$("#banner-slider").slick({
		infinite: true,
		speed: 9000,
		fade: true,
		  responsive: [
		    {
		      breakpoint: 767,
		      settings: {
		        dots: true
		      }
		    }
		  ]
	});


	$("#banner-slider").on('beforeChange', function(event, slick, currentSlide, nextSlide){
		console.log( nextSlide );

		$('.lsh-plaza .plaza-img').removeClass('active');
		$('.lsh-plaza .plaza-img').eq( nextSlide ).addClass('active');

		$('#section1 .contein-tabs .elite-property').removeClass('active');
		$('#section1 .contein-tabs .elite-property').eq( nextSlide ).addClass('active');

	});

//-------------------------------------------------------------------------------------------
	$(".commercial-tab-links ul li").on('click', function(){
		slideNumber = $(this).index();
		isPause = true;
		setTimeout(function(){
			isPause = false;
		}, 2000);

		$('.commercial-tab-links li').find('span.after').css({
			width: 0+"%"
		});

		if ( slideNumber > 0 ){

			for ( var j=0; j < slideNumber ; j++ ){
				$('.commercial-tab-links li').eq(j).find('span.after').css({
					width: 100+"%"
				});
			}

		}

		resetProgressbar();
		startProgressbar( slideNumber );

		$('#horizontal-slider').slick('slickGoTo', slideNumber );


		$(".commercial-tab-links ul li").removeClass('active');
		$(".commercial-tabs-content .commercial-tab1").removeClass('active');

		$(".commercial-tab-links ul li").eq(slideNumber).addClass('active');
		$(".commercial-tabs-content .commercial-tab1").eq(slideNumber).addClass('active');
	});

    
    $(".brand_slide").slick({
		infinite: true,
		speed: 1000,
		autoplay: false,
		autoplaySpeed: 500,
		
		responsive: [
			{
			breakpoint: 767,
			settings: {
				dots: true
			}
			}
		]
	});
    
	$("#horizontal-slider").slick({
		infinite: true,
		speed: 1000
	});
	//Progress Bar
	var time = 3;

	$bar = $('.commercial-tab-links li span.after');

	function startProgressbar(  ) {
		resetProgressbar();
		percentTime = 0 ;
		tick = setInterval( function(){
			interval( ) ;
		}, 10);
	}

	function interval( ) {
		if(isPause === false) {
			percentTime += 1 / (time+0.1);

			console.log( slideNumber );

			$('.commercial-tab-links li span.after').eq(slideNumber).css({
				width: percentTime+"%"
			});

			if(percentTime >= 100){
				if(slideNumber == 2){
					slideNumber = -1;
					$bar.css({
						width: 0+'%' 
					});
				}
				//$.fn.fullpage.moveSlideRight();
				//$("#horizontal-slider").slick('slickGoTo',slideNumber);
				$("#horizontal-slider .slick-next").trigger('click');

				startProgressbar(  );

				$(".commercial-tab-links ul li").removeClass('active');
				$(".commercial-tabs-content .commercial-tab1").removeClass('active');
				slideNumber++;
				$(".commercial-tab-links ul li").eq(slideNumber).addClass('active');
				$(".commercial-tabs-content .commercial-tab1").eq(slideNumber).addClass('active');
			}
		}
	}

	function resetProgressbar() {
		clearTimeout(tick);
	}
	/*function resetProgressbar2() {
		clearTimeout(tick);
		$bar.css({
			width: 0+'%' 
		});
	}*/
	//startProgressbar();	
	function createFullpage(){
		$('#fullpage').fullpage({
			//Navigation
			menu: '#menu',
			anchors:['home', 'welcome', 'properties', 'about-us', 'countries', 'news', 'footer'],
			loopHorizontal: true,
			loopBottom: false,

			verticalCentered:true,
			continuousVertical: false,
			loopTop: false,
			fitToSection: true,
			
	        controlArrows: false,
			resetSliders: true,
			resetSlidersKey: 'YWx2YXJvdHJpZ28uY29tX3R0U2NtVnpaWFJUYkdsa1pYSnozZHQ=',
			onLeave: function(index, nextIndex, direction){

				String.prototype.count=function(s1) {   
					return (this.length - this.replace(new RegExp(s1,"g"), '').length) / s1.length;  
				}
				
				if(navigator.appVersion.indexOf("MSIE 9.") === -1){
					var transform = $('#fullpage').css('transform').split(/[()]/)[1];
					var matrixSize = transform.count(',');
					if (matrixSize === 5) {
						var transformX = parseInt(transform.split(',')[5],10);
					}
					if (matrixSize === 15) {
						var transformX = parseInt(transform.split(',')[13],10);
					};
				} else {
					var topOffset = parseInt($('#fullpage').css('top'));
				}



				var leavingSection = $(this).attr('data-currsection');
				//after leaving section 2
				if(index == 2 && direction =='down'){
					//resetProgressbar2();
					console.log(' to 3 down')
					isPause = false;
					startProgressbar();
				}
				//after leaving section 4
				else if(index == 4 && direction == 'up'){
					//resetProgressbar2();
					console.log(' to 3 up ')
					isPause = false;
					startProgressbar();
				}
				else {
					//resetProgressbar2();
					isPause = true;
				}

				var translater =  - $(window).height()/2 + $('.project-developement').height()/2 ;

				if(index == 3 && direction =='down'){
					console.log(' to 3 down');

				}
				//after leaving section 4
				else if(index == 5 && direction == 'up'){


					
					console.log(' to 3 up ')
				}


				
			},
			afterLoad: function(anchorLink, index){
				if(anchorLink == 'properties'){
					isPause = false;
					startProgressbar();
				}
			}	
		});
	}
	fullpagecheck1();
	$(window).resize(fullpagecheck2);
	function fullpagecheck1(){
		if ($(window).width() > 1) {
			createFullpage();
		}
	}
	function fullpagecheck2(){
		if ($(window).width() <= 1 &&  $('html').hasClass('fp-enabled')) {
			// console.log("not fullpage");
			$.fn.fullpage.destroy('all');
		}
		if ($(window).width() > 767 && !($('html').hasClass('fp-enabled'))) {
			// console.log("fullpage");
			createFullpage();
		}
	}
	/*******************
		Banner Canvas
	********************/
    // settings
    var divisions = 3;
    var duration = 30000; // in ms
    var canvas = document.getElementById("progresscanvas");
    var context = canvas.getContext("2d");
    var x = canvas.width / 1;
    var y = canvas.height / 1;
    var radius = (canvas.width / 7) * 1;
    context.lineWidth = 1;
	var canvas3 = document.getElementById('dotimagecanvas'),
        ctx3 = canvas3.getContext('2d');
    // init
    var currentSplit = 0;
    var splitAngle = (Math.PI * 2) / divisions;
    var splitTime = (duration / (divisions*2)); // how much time per split per end
    var angles = [0,0]; // here we store both start and end angle
    var current = 0;
    var startTime = performance.now();
    draw();
    //For images on dottedcanvas
    var dotxpos = 20;
    var dotypos = 20;
    var dotxpos2 = 60;
    var dotypos2 = 20;
    var dotxpos3 = 100;
    var dotypos3 = 20;
    function draw(currentTime) {
      // first convert the elapsed time to an angle
      var timedAngle =  ((currentTime - startTime) / splitTime) * splitAngle;
      // set the current end to this timed angle + the current position on the circle
      angles[current] = timedAngle + (splitAngle * currentSplit);
      if (timedAngle >= splitAngle) {  // one split is done for this end
        // it should not go farther than the threshold
        angles[current] = (splitAngle * (currentSplit + 1));
        current = +(!current) // switch which end should move
        startTime = currentTime; // reset the timer
        if(!current){ // we go back to the start
            currentSplit = (currentSplit + 1) % divisions; // increment our split index
            //console.log(x + radius, y);
            //console.log(x + radius*Math.cos(angles[0]), y + radius*Math.sin(angles[1]));  //Position of ending points of each arc 
            //Update dot images after each strip drawn
            var dotactive = document.getElementById("dotactive");   
            var dotnormal = document.getElementById("dotnormal"); 
            dotcanvasset();
            if(currentSplit == 0){
            	ctx3.clearRect(0, 0, canvas3.width, canvas3.height); 
                ctx3.drawImage(dotactive, dotxpos - 16, dotypos - 16, 32, 32);
                ctx3.drawImage(dotnormal, dotxpos2 - 16, dotypos2 - 16, 32, 32);
                ctx3.drawImage(dotnormal, dotxpos3 - 16, dotypos3 - 16, 32, 32);
				//Draw Text for 0th revolution
				ctx3.fillStyle = "white";
				ctx3.textAlign="center"; 
				ctx3.font = "72px/80px crimson_textroman";
				/*
				ctx3.fillText("ELITE PROPERTY", 640, 400);
				ctx3.font = "40px/50px crimson_textroman";
				ctx3.fillText("DEVELOPER 1", 640, 450);    
				*/
            }else if(currentSplit == 1){
            	ctx3.clearRect(0, 0, canvas3.width, canvas3.height); 
                ctx3.drawImage(dotnormal, dotxpos - 16, dotypos - 16, 32, 32);
                ctx3.drawImage(dotactive, dotxpos2 - 16, dotypos2 - 16, 32, 32);
                ctx3.drawImage(dotnormal, dotxpos3 - 16, dotypos3 - 16, 32, 32);
				//Draw Text for 1st revolution
				ctx3.fillStyle = "white";
				ctx3.textAlign="center"; 
				ctx3.font = "72px/80px crimson_textroman";
				/*
				ctx3.fillText("TEST PROPERTY", 640, 400);
				ctx3.font = "40px/50px crimson_textroman";
				ctx3.fillText("DEVELOPDERS 2", 640, 450);     
				*/               
            }else if(currentSplit == 2){
            	ctx3.clearRect(0, 0, canvas3.width, canvas3.height); 
                ctx3.drawImage(dotnormal, dotxpos - 16, dotypos - 16, 32, 32);
                ctx3.drawImage(dotnormal, dotxpos2 - 16, dotypos2 - 16, 32, 32);
                ctx3.drawImage(dotactive, dotxpos3 - 16, dotypos3 - 16, 32, 32);
				//Draw Text for 2nd revolution
				ctx3.fillStyle = "white";
				ctx3.textAlign="center"; 
				/*
				ctx3.font = "72px/80px crimson_textroman";
				ctx3.fillText("ELITE PROPERTY", 640, 400);
				ctx3.font = "40px/50px crimson_textroman";
				ctx3.fillText("DEVELOPERS 3", 640, 450); 
				*/                   
            }    
            $("#banner-slider .slick-next").trigger('click');                           
        }
      }
      if(angles[1] > Math.PI*2){ // we finished one complete revolution
        angles[0] = angles[1] = current = 0; // reset everything
      }
      
      // at every frame we clear everything
      context.clearRect(0, 0, canvas.width, canvas.height);
      // and redraw
      context.beginPath();
      context.arc(x, y, radius, angles[0], angles[1], true);
      context.strokeStyle = '#e6b269';
      context.stroke();
      requestAnimationFrame(draw); // loop at screen refresh rate
    } 
    //Draw dotted Circle
    function calcPointsCirc( cx,cy, rad, dashLength)
    {
        var n = rad/dashLength,
            alpha = Math.PI * 2 / n,
            pointObj = {},
            points = [],
            i = -1;
            
        while( i < n )
        {
            var theta = alpha * i,
                theta2 = alpha * (i+1);
            
            points.push({x : (Math.cos(theta) * rad) + cx, y : (Math.sin(theta) * rad) + cy, ex : (Math.cos(theta2) * rad) + cx, ey : (Math.sin(theta2) * rad) + cy});
       i+=2;
        }              
        return points;            
    }
    var canvas2 = document.getElementById('dottedcanvas'),
    ctx = canvas2.getContext('2d');
    function dotcanvasset(){
      ctx.clearRect(0, 0, canvas2.width, canvas2.height); 
      var pointArray= calcPointsCirc(x,y,radius, 1);
      ctx.strokeStyle = "rgb(255,255,255)";
      ctx.lineWidth = 0.5;
      ctx.beginPath();
      for(p = 0; p < pointArray.length; p++){
          ctx.moveTo(pointArray[p].x, pointArray[p].y);
          ctx.lineTo(pointArray[p].ex, pointArray[p].ey);
          
          ctx.stroke();
      }
      ctx.closePath(); 
	  ctx.lineWidth = 0.5;
	  ctx.strokeStyle = "rgba(255,255,255,0.5)";
      /*
      
      
      ctx.beginPath();
      ctx.arc(x,y,radius + 250,0,2*Math.PI);   
      ctx.stroke();
      ctx.closePath(); 
      */
		//Draw Text for the initial load
		ctx3.clearRect(0, 0, canvas3.width, canvas3.height); 
		ctx3.fillStyle = "white";
		ctx3.textAlign="center"; 
		/*
		ctx3.font = "72px/80px crimson_textroman";
		ctx3.fillText("ELITE PROPERTY", 640, 400);
		ctx3.font = "40px/50px crimson_textroman";
		ctx3.fillText("DEVELOPER 1", 640, 450); 
		*/     
    }
    dotcanvasset();
    //Draw dot images
    window.onload = function drawdots(cs) {
        var dotactive = document.getElementById("dotactive");   
        var dotnormal = document.getElementById("dotnormal");  
        ctx3.drawImage(dotnormal, dotxpos - 16, dotypos - 16, 32, 32);
        ctx3.drawImage(dotnormal, dotxpos2 - 16, dotypos2 - 16, 32, 32);
        ctx3.drawImage(dotnormal, dotxpos3 - 16, dotypos3 - 16, 32, 32);      
		function checkHorSlider(){
			if($(window).width() < 768){
				isPause = false;
				startProgressbar();
			}else{
				isPause = true;
				$(".fs-lg").css({'height': $(".fp-section").outerHeight()});
			}
		}
		checkHorSlider();
		$(window).resize(checkHorSlider);        
    }
});

  }
});

// END MOBILE

//PC
$(document).ready(function() {
  if(document.documentElement.clientWidth > 767){

jQuery(document).ready(function($) {
	var $bar,
	  tick,
	  isPause,
	  percentTime,
	  slideNumber = 0;
	
	//Banner slider
	$("#banner-slider").slick({
		infinite: true,
		speed: 9000,
		fade: true,
		  responsive: [
		    {
		      breakpoint: 767,
		      settings: {
		        dots: true
		      }
		    }
		  ]
	});


	$("#banner-slider").on('beforeChange', function(event, slick, currentSlide, nextSlide){
		console.log( nextSlide );

		$('.lsh-plaza .plaza-img').removeClass('active');
		$('.lsh-plaza .plaza-img').eq( nextSlide ).addClass('active');

		$('#section1 .contein-tabs .elite-property').removeClass('active');
		$('#section1 .contein-tabs .elite-property').eq( nextSlide ).addClass('active');

	});

//-------------------------------------------------------------------------------------------
	$(".commercial-tab-links ul li").on('click', function(){
		slideNumber = $(this).index();
		isPause = true;
		setTimeout(function(){
			isPause = false;
		}, 2000);

		$('.commercial-tab-links li').find('span.after').css({
			width: 0+"%"
		});

		if ( slideNumber > 0 ){

			for ( var j=0; j < slideNumber ; j++ ){
				$('.commercial-tab-links li').eq(j).find('span.after').css({
					width: 100+"%"
				});
			}

		}

		resetProgressbar();
		startProgressbar( slideNumber );

		$('#horizontal-slider').slick('slickGoTo', slideNumber );


		$(".commercial-tab-links ul li").removeClass('active');
		$(".commercial-tabs-content .commercial-tab1").removeClass('active');

		$(".commercial-tab-links ul li").eq(slideNumber).addClass('active');
		$(".commercial-tabs-content .commercial-tab1").eq(slideNumber).addClass('active');
	});

    
    $(".brand_slide").slick({
		infinite: true,
		speed: 1000,
		autoplay: false,
		autoplaySpeed: 500,
		
		responsive: [
			{
			breakpoint: 767,
			settings: {
				dots: true
			}
			}
		]
	});
    
	$("#horizontal-slider").slick({
		infinite: true,
		speed: 1000
	});
	//Progress Bar
	var time = 3;

	$bar = $('.commercial-tab-links li span.after');

	function startProgressbar(  ) {
		resetProgressbar();
		percentTime = 0 ;
		tick = setInterval( function(){
			interval( ) ;
		}, 10);
	}

	function interval( ) {
		if(isPause === false) {
			percentTime += 1 / (time+0.1);

			console.log( slideNumber );

			$('.commercial-tab-links li span.after').eq(slideNumber).css({
				width: percentTime+"%"
			});

			if(percentTime >= 100){
				if(slideNumber == 2){
					slideNumber = -1;
					$bar.css({
						width: 0+'%' 
					});
				}
				//$.fn.fullpage.moveSlideRight();
				//$("#horizontal-slider").slick('slickGoTo',slideNumber);
				$("#horizontal-slider .slick-next").trigger('click');

				startProgressbar(  );

				$(".commercial-tab-links ul li").removeClass('active');
				$(".commercial-tabs-content .commercial-tab1").removeClass('active');
				slideNumber++;
				$(".commercial-tab-links ul li").eq(slideNumber).addClass('active');
				$(".commercial-tabs-content .commercial-tab1").eq(slideNumber).addClass('active');
			}
		}
	}

	function resetProgressbar() {
		clearTimeout(tick);
	}
	/*function resetProgressbar2() {
		clearTimeout(tick);
		$bar.css({
			width: 0+'%' 
		});
	}*/
	//startProgressbar();	
	function createFullpage(){
		$('#fullpage').fullpage({
			//Navigation
			menu: '#menu',
			anchors:['home', 'welcome', 'properties', 'about-us', 'countries', 'news', 'footer'],
			loopHorizontal: true,
			loopBottom: false,

			verticalCentered:true,
			continuousVertical: false,
			loopTop: false,
			fitToSection: true,
			
	        controlArrows: false,
			resetSliders: true,
			resetSlidersKey: 'YWx2YXJvdHJpZ28uY29tX3R0U2NtVnpaWFJUYkdsa1pYSnozZHQ=',
			onLeave: function(index, nextIndex, direction){

				String.prototype.count=function(s1) {   
					return (this.length - this.replace(new RegExp(s1,"g"), '').length) / s1.length;  
				}
				
				if(navigator.appVersion.indexOf("MSIE 9.") === -1){
					var transform = $('#fullpage').css('transform').split(/[()]/)[1];
					var matrixSize = transform.count(',');
					if (matrixSize === 5) {
						var transformX = parseInt(transform.split(',')[5],10);
					}
					if (matrixSize === 15) {
						var transformX = parseInt(transform.split(',')[13],10);
					};
				} else {
					var topOffset = parseInt($('#fullpage').css('top'));
				}



				var leavingSection = $(this).attr('data-currsection');
				//after leaving section 2
				if(index == 2 && direction =='down'){
					//resetProgressbar2();
					console.log(' to 3 down')
					isPause = false;
					startProgressbar();
				}
				//after leaving section 4
				else if(index == 4 && direction == 'up'){
					//resetProgressbar2();
					console.log(' to 3 up ')
					isPause = false;
					startProgressbar();
				}
				else {
					//resetProgressbar2();
					isPause = true;
				}

				var translater =  - $(window).height()/2 + $('.project-developement').height()/2 ;

				if(index == 3 && direction =='down'){
					console.log(' to 3 down');

				}
				//after leaving section 4
				else if(index == 5 && direction == 'up'){


					
					console.log(' to 3 up ')
				}


				
			},
			afterLoad: function(anchorLink, index){
				if(anchorLink == 'properties'){
					isPause = false;
					startProgressbar();
				}
			}	
		});
	}
	fullpagecheck1();
	$(window).resize(fullpagecheck2);
	function fullpagecheck1(){
		if ($(window).width() > 767) {
			createFullpage();
		}
	}
	function fullpagecheck2(){
		if ($(window).width() <= 767 &&  $('html').hasClass('fp-enabled')) {
			// console.log("not fullpage");
			$.fn.fullpage.destroy('all');
		}
		if ($(window).width() > 767 && !($('html').hasClass('fp-enabled'))) {
			// console.log("fullpage");
			createFullpage();
		}
	}
	/*******************
		Banner Canvas
	********************/
    // settings
    var divisions = 3;
    var duration = 30000; // in ms
    var canvas = document.getElementById("progresscanvas");
    var context = canvas.getContext("2d");
    var x = canvas.width / 2;
    var y = canvas.height / 2;
    var radius = (canvas.width / 7) * 2;
    context.lineWidth = 2;
	var canvas3 = document.getElementById('dotimagecanvas'),
        ctx3 = canvas3.getContext('2d');
    // init
    var currentSplit = 0;
    var splitAngle = (Math.PI * 2) / divisions;
    var splitTime = (duration / (divisions*2)); // how much time per split per end
    var angles = [0,0]; // here we store both start and end angle
    var current = 0;
    var startTime = performance.now();
    draw();
    //For images on dottedcanvas
    var dotxpos = 1005.7142857142858;
    var dotypos = 383.4999999999999;
    var dotxpos2 = 457.1428571428572;
    var dotypos2 = 700.2178619554519;
    var dotxpos3 = 457.142857142857;
    var dotypos3 = 66.78213804454822;
    function draw(currentTime) {
      // first convert the elapsed time to an angle
      var timedAngle =  ((currentTime - startTime) / splitTime) * splitAngle;
      // set the current end to this timed angle + the current position on the circle
      angles[current] = timedAngle + (splitAngle * currentSplit);
      if (timedAngle >= splitAngle) {  // one split is done for this end
        // it should not go farther than the threshold
        angles[current] = (splitAngle * (currentSplit + 1));
        current = +(!current) // switch which end should move
        startTime = currentTime; // reset the timer
        if(!current){ // we go back to the start
            currentSplit = (currentSplit + 1) % divisions; // increment our split index
            //console.log(x + radius, y);
            //console.log(x + radius*Math.cos(angles[0]), y + radius*Math.sin(angles[1]));  //Position of ending points of each arc 
            //Update dot images after each strip drawn
            var dotactive = document.getElementById("dotactive");   
            var dotnormal = document.getElementById("dotnormal"); 
            dotcanvasset();
            if(currentSplit == 0){
            	ctx3.clearRect(0, 0, canvas3.width, canvas3.height); 
                ctx3.drawImage(dotactive, dotxpos - 16, dotypos - 16, 32, 32);
                ctx3.drawImage(dotnormal, dotxpos2 - 16, dotypos2 - 16, 32, 32);
                ctx3.drawImage(dotnormal, dotxpos3 - 16, dotypos3 - 16, 32, 32);
				//Draw Text for 0th revolution
				ctx3.fillStyle = "white";
				ctx3.textAlign="center"; 
				ctx3.font = "72px/80px crimson_textroman";
				/*
				ctx3.fillText("ELITE PROPERTY", 640, 400);
				ctx3.font = "40px/50px crimson_textroman";
				ctx3.fillText("DEVELOPER 1", 640, 450);    
				*/
            }else if(currentSplit == 1){
            	ctx3.clearRect(0, 0, canvas3.width, canvas3.height); 
                ctx3.drawImage(dotnormal, dotxpos - 16, dotypos - 16, 32, 32);
                ctx3.drawImage(dotactive, dotxpos2 - 16, dotypos2 - 16, 32, 32);
                ctx3.drawImage(dotnormal, dotxpos3 - 16, dotypos3 - 16, 32, 32);
				//Draw Text for 1st revolution
				ctx3.fillStyle = "white";
				ctx3.textAlign="center"; 
				ctx3.font = "72px/80px crimson_textroman";
				/*
				ctx3.fillText("TEST PROPERTY", 640, 400);
				ctx3.font = "40px/50px crimson_textroman";
				ctx3.fillText("DEVELOPDERS 2", 640, 450);     
				*/               
            }else if(currentSplit == 2){
            	ctx3.clearRect(0, 0, canvas3.width, canvas3.height); 
                ctx3.drawImage(dotnormal, dotxpos - 16, dotypos - 16, 32, 32);
                ctx3.drawImage(dotnormal, dotxpos2 - 16, dotypos2 - 16, 32, 32);
                ctx3.drawImage(dotactive, dotxpos3 - 16, dotypos3 - 16, 32, 32);
				//Draw Text for 2nd revolution
				ctx3.fillStyle = "white";
				ctx3.textAlign="center"; 
				/*
				ctx3.font = "72px/80px crimson_textroman";
				ctx3.fillText("ELITE PROPERTY", 640, 400);
				ctx3.font = "40px/50px crimson_textroman";
				ctx3.fillText("DEVELOPERS 3", 640, 450); 
				*/                   
            }    
            $("#banner-slider .slick-next").trigger('click');                           
        }
      }
      if(angles[1] > Math.PI*2){ // we finished one complete revolution
        angles[0] = angles[1] = current = 0; // reset everything
      }
      
      // at every frame we clear everything
      context.clearRect(0, 0, canvas.width, canvas.height);
      // and redraw
      context.beginPath();
      context.arc(x, y, radius, angles[0], angles[1], true);
      context.strokeStyle = '#e6b269';
      context.stroke();
      requestAnimationFrame(draw); // loop at screen refresh rate
    } 
    //Draw dotted Circle
    function calcPointsCirc( cx,cy, rad, dashLength)
    {
        var n = rad/dashLength,
            alpha = Math.PI * 2 / n,
            pointObj = {},
            points = [],
            i = -1;
            
        while( i < n )
        {
            var theta = alpha * i,
                theta2 = alpha * (i+1);
            
            points.push({x : (Math.cos(theta) * rad) + cx, y : (Math.sin(theta) * rad) + cy, ex : (Math.cos(theta2) * rad) + cx, ey : (Math.sin(theta2) * rad) + cy});
       i+=2;
        }              
        return points;            
    }
    var canvas2 = document.getElementById('dottedcanvas'),
    ctx = canvas2.getContext('2d');
    function dotcanvasset(){
      ctx.clearRect(0, 0, canvas2.width, canvas2.height); 
      var pointArray= calcPointsCirc(x,y,radius, 1);
      ctx.strokeStyle = "rgb(255,255,255)";
      ctx.lineWidth = 0.5;
      ctx.beginPath();
      for(p = 0; p < pointArray.length; p++){
          ctx.moveTo(pointArray[p].x, pointArray[p].y);
          ctx.lineTo(pointArray[p].ex, pointArray[p].ey);
          
          ctx.stroke();
      }
      ctx.closePath(); 
	  ctx.lineWidth = 0.5;
	  ctx.strokeStyle = "rgba(255,255,255,0.5)";
      /*
      
      
      ctx.beginPath();
      ctx.arc(x,y,radius + 250,0,2*Math.PI);   
      ctx.stroke();
      ctx.closePath(); 
      */
		//Draw Text for the initial load
		ctx3.clearRect(0, 0, canvas3.width, canvas3.height); 
		ctx3.fillStyle = "white";
		ctx3.textAlign="center"; 
		/*
		ctx3.font = "72px/80px crimson_textroman";
		ctx3.fillText("ELITE PROPERTY", 640, 400);
		ctx3.font = "40px/50px crimson_textroman";
		ctx3.fillText("DEVELOPER 1", 640, 450); 
		*/     
    }
    dotcanvasset();
    //Draw dot images
    window.onload = function drawdots(cs) {
        var dotactive = document.getElementById("dotactive");   
        var dotnormal = document.getElementById("dotnormal");  
        ctx3.drawImage(dotnormal, dotxpos - 16, dotypos - 16, 32, 32);
        ctx3.drawImage(dotnormal, dotxpos2 - 16, dotypos2 - 16, 32, 32);
        ctx3.drawImage(dotnormal, dotxpos3 - 16, dotypos3 - 16, 32, 32);      
		function checkHorSlider(){
			if($(window).width() < 768){
				isPause = false;
				startProgressbar();
			}else{
				isPause = true;
				$(".fs-lg").css({'height': $(".fp-section").outerHeight()});
			}
		}
		checkHorSlider();
		$(window).resize(checkHorSlider);        
    }
});

  }
});

//END PC